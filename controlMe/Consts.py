#A list of constants often used in the code

appName = "ControlMeApp"
appCred = "data/appCred.dat"
accountCred = "data/accountCred.dat"
apiBaseURLFile = "data/apiBaseURL.dat"
botNameFile = "data/botName.dat"
emergencyContactFile = "data/emergencyContact.dat"

proposeEmergencyContact = True
emergencyMessage = "Hey !\n J'ai rencontré une erreur récemment... Voilà, pour te mettre au courant... Bon courage !"
verboseBase = 0
verboseError = 10

debug = False
nameMaxLength = 32
tootLength = 500

log = "data/log/log.txt"

botName = "controlMe"



from pynput.keyboard import Key
#See https://pythonhosted.org/pynput/_modules/pynput/keyboard/_base.html#Key for a list of special keys
allowedKeys = {"z" : 'z', "q": 'q', "s" : 's', "d": 'd', "jump" : Key.space,
 "left": 'q', "right" : 'd', "up": 'z', "down" : "s", "a":'a', "b": 'b'}
