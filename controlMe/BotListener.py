import mastodon

import Consts, Log
from TootHTMLParser import TootHTMLParser

from pynput.keyboard import Key, Controller


#a Mastodon Stream Listener  defines functions to react when something happens on Mastodon. We inherit it.
class BotListener(mastodon.StreamListener):
    def __init__(self, bot):
        self.bot = bot
        self.keyboard = Controller()

    #What happens if the bot receives a notification ? We only want to react to DM.
    def on_notification(self, notification):
        #We react to mentions only
        if(notification['type'] != 'mention'):
            Log.logprint("nevermind, it's not a mention")
            return

        #So there is a toot !
        status = notification['status']

        if(status["visibility"] == "direct"): #we only accept DM
            #And there is a text in this toot. But it is mixed with HTML we don't want so we get rid of it.
            content = str(status["content"])
            parser = TootHTMLParser()
            parser.feed(content)
            content = (parser.txt).strip()

            #We don't care about the mention of the bot.
            content = content.replace("@"+Consts.botName, "").strip()
            Log.logprint(content)

            #If we find one of the allowed commands, we press the list of corresponding keys.
            #See Consts.allowedKeys for a dict of allowed commands and the list of corresponding keys.
            for command, keys in Consts.allowedKeys.items():
                if(content == command):
                    Log.logprint(command)
                    keys = list(keys)
                    for key in keys:
                        self.keyboard.press(key)
                        self.keyboard.release(key)
