import mastodon
import os

import Consts, Log
from BotListener import BotListener


#The bot connects to the mastondon api and will use a stream listener to wait for notifications.
class Bot(object):
    def __init__(self):
        #First we can connect using the api.
        api_base_url = open(Consts.apiBaseURLFile).read()
        self.apiMasto = mastodon.Mastodon(client_id = Consts.appCred, access_token = Consts.accountCred,api_base_url = api_base_url)
        Log.logprint("Bot connected.")

        self.listener = BotListener(self)


    #We just stream the notifications. Never ending function (in theory)
    def listen(self):
        Log.logprint("Now listening for new events...")
        self.apiMasto.stream_user(self.listener)

    #In case of an error, contact the emergency contact via Mastodon ?
    def emergencyContact(self):
        contact = open(Consts.emergencyContactFile).read()
        self.apiMasto.status_post('@' + contact + " " + Consts.emergencyMessage, visibility = 'direct')
