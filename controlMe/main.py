#controlMe-bot
#send a key via a mastodon direct message to the bot and it will press the key on your keyboard
#see the allowedKeys dict in Consts for a list of allowed keys.

import time
import traceback
import os


import  Consts, Log, messages
from Bot import Bot

#Go to the right folder
cur_dir = os.path.dirname(__file__)
if len(cur_dir) == 0:
    cur_dir = os.getcwd()

os.chdir(cur_dir)


#Before doing anything we check if the configuration is OK :
#Do we have tokens for the app, for the bot, do we have an emergency contact ?
#If not, we wait for informations given by the user.
messages.verifyConfig()



#creation of a bot that will always listen for new toots (and react to them...)
bot = Bot()

#The main loop is nothing else than a while True : listen to new toots.
#If an error happens we send a message to the emergency contact if it exists (only once)
# and the error is written in the log. The bot will continue to listen after that so it's not a big deal usually.
noErrors = True
while True:
    try:
        bot.listen()
    except Exception:
        #Write error in the log.
        Log.logprint(traceback.format_exc(), Consts.verboseError)

        #Maybe contact the emergency contact.
        if(noErrors and os.path.exists(Consts.emergencyContactFile)):
            try:
                bot.emergencyContact()
            except Exception:
                Log.logprint("[ERROR] Couldn't contact emergency contact without throwing an error", Consts.verboseError)
            noErrors = False

        #"Reboot" after some time
        Log.logprint("I will try to reconnect in 5 seconds...", Consts.verboseError)
        time.sleep(5)
