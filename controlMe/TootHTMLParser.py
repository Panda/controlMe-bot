from html.parser import HTMLParser


#We just use this to get rid of the HTML in the toots we receive.
#Basically it will just keep the real data and do nothing with everything HTML-related.
class TootHTMLParser(HTMLParser):
    def __init__(self):
        super().__init__()
        self.txt = ""
    def handle_data(self, data):
        self.txt += str(data)
