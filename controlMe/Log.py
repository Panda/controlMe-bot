import Consts
import time

#Just a better print function. We log some messages into a file.
#Do also regular prints if the constants are accordingly set.
def logprint(text, verbose = Consts.verboseBase):
    text = str(time.asctime( time.localtime(time.time()) )) + " : " + str(text) + "\n\n"
    if(verbose >= Consts.verboseError):
        with open(Consts.log, 'a') as output:
            output.write(text)
    if(Consts.debug):
        print(text)
